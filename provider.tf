provider "aws" {
  region     = "eu-west-3"
  access_key = file("../access.txt")
  secret_key = file("../secret.txt")
}
